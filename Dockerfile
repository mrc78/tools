FROM alpine:3.10
RUN apk add --no-cache \
        httpie \
        curl \
        drill \
        tcpdump \
        lsof \
        strace \
        nmap \
        jq

